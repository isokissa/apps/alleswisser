
export type Question = {
    id: number,
    parentId: number,
    question: string,
    yesId: number, 
    noId: number
}

export type GetQuestionFn = (id: number) => Promise<Question|null>
