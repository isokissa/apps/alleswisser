import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ApiService } from '../src/api/api.service';
import { Question } from '../../libs/common/types';
import { questions } from './test-questions';

describe('ApiController (e2e)', () => {
  let app: INestApplication;
  let dummyApiService = {
    getQuestion: (id: number) => {
      return questions[id];
    }
  }
  
  
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ApiService)
      .useValue(dummyApiService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/moj (GET)', () => {
    const id = 0;
    const expected: Question = questions[id];
    return request(app.getHttpServer())
      .get('/api/questions/' + id)
      .expect(200)
      .expect(expected);
  });
});
