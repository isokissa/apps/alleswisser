import { Question } from '../../libs/common/types';

type DummyDatabase = {
    [id: number]: Question
}

export const questions: DummyDatabase = {
    0: {
        id: 0,
        question: 'elephant',
        parentId: null,
        yesId: null,
        noId: null
      },
} 
