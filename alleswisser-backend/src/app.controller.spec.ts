import { InternalServerErrorException } from '@nestjs/common/exceptions/internal-server-error.exception';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  it("has a landing method which does nothing", () => {
    expect(appController.landing());
  })
});
