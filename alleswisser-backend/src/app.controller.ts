import { Controller, Get, Redirect, HttpStatus, Res, InternalServerErrorException } from '@nestjs/common'
import { Response } from 'express'

@Controller()
export class AppController {
  @Get()
  @Redirect('/ui/index.html')
  landing() {}
}
