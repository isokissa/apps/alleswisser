import { Controller, Get, Param } from '@nestjs/common';
import { ApiService } from './api.service';
import { Question } from '../../../libs/common/types';


@Controller('api')
export class ApiController {
    constructor(private readonly appService: ApiService) {}

    @Get('questions/:id')
    getQuestion(@Param('id') id: number): Question {
      return this.appService.getQuestion(id)
    }
}
