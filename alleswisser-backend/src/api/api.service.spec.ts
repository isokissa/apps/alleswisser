import { Test, TestingModule } from '@nestjs/testing';
import { ApiService } from './api.service';
import { Question } from '../common/types';


describe('ApiService', () => {
  let service: ApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiService],
    }).compile();

    service = module.get<ApiService>(ApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('returns first question', () => {
    const id = 0
    expect(service.getQuestion(id)).toEqual<Question>({
      id: id, 
      question: 'elephant',
      parentId: null,
      yesId: null,
      noId: null
    })
  })
});
