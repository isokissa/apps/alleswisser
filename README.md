# Alleswisser

The idea is to create an all-knowing application that will be eventually be
able to guess anything that user can imagine, by asking good questions. Sometimes, 
by mirracle it can happen that the application will not succeed in guessing the thing.
Then it will learn the new thing, and next time it will be able to guess also that thing. 

The technologies that are supposed to be used are TypeScript at both front- and
backend: 
- React at frontend 
- NestJS at backend
- MariaDB as a database

The idea is also to serve React fronend contents as a static contents from NestJS application. 


## Development environment

It is recommended to run the development environment as a docker container, in order
to keep the host machine clean from numerous development tools. 

In home directory of the project run: 

    docker compose build

and then: 

    docker compose run

This will start frontend and backend servers, and the application's frontend will be available
at `localhost:3000`. 

Probably is very interesting to access the development environment to run unit, integration, or
e2e tests. For that, you can just connect to a running development container, for example: 

    docker exec -it alleswisser-frontend bash

and then from inside container you can run all the testing scripts, like: 

    npm test

You can of course also manually startup any of the containers with `docker run`, but if you just
connect to one of one of those that are started by `docker compose`, it is handy, because all 
the volumes and ports are already correctly configured. 

## Deployment

Currently it is done totally manually.  

    cd alleswisser-backend
    scp -rp dist client package.json isokissa@isokissa.org:isokissa.alleswisser.site/
