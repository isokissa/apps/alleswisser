import React, { Children } from 'react'

import { ComponentStory, ComponentMeta } from '@storybook/react'
import Layout from './Layout'

export default {
  title: 'Components/Layout',
  component: Layout
} as ComponentMeta<typeof Layout>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof Layout> = (args) => <Layout page={args.page} />

export const Empty = Template.bind({})
Empty.args = {
    page: <p></p>
}

export const SingleLine = Template.bind({})
SingleLine.args = {
  page: <div>Bez paragrafa
    <p>Da vidimo da li ume da radi kao react</p>
    <p>Drugi paragraf</p>
  </div>
}