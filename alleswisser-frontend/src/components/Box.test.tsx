import React from 'react';
import { render, screen } from '@testing-library/react';
import Box from './Box';

test('renders learn react link', () => {
  render(<Box>test contents</Box>);
  const linkElement = screen.getByText(/test contents/i);
  expect(linkElement).toBeInTheDocument();
});
