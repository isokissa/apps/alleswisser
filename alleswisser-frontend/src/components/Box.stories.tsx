import React, { Children } from 'react'

import { ComponentStory, ComponentMeta } from '@storybook/react'
import Box from './Box'

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Components/Box',
  component: Box
} as ComponentMeta<typeof Box>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof Box> = (args) => <Box>{args.children}</Box>

export const Empty = Template.bind({})
Empty.args = {
    children: ""
}

export const SingleLine = Template.bind({})
SingleLine.args = {
  children: <div>Bez paragrafa
    <p>Da vidimo da li ume da radi kao react</p>
    <p>Drugi paragraf</p>
  </div>
}