import React, { ReactNode } from 'react'
import styles from './Box.module.css'

type BoxProps = {
    children: ReactNode
}

function Box({children}: BoxProps): JSX.Element {
    return <div className={styles.basic}>{children}</div>
}

export default Box