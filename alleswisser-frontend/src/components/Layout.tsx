import React, { ReactNode } from 'react'
import styles from './Layout.module.css'
import Logo from '../assets/AlleswisserLogo.svg'

type LayoutProps = {
    page: ReactNode
}

function Box({ page }: LayoutProps): JSX.Element {
    return <div className={styles.basic}>
        <nav className={styles.headerNav}>
            <img src={Logo} alt="logo"/>
            <span className={styles.menuOption}>Quiz</span>
            <span className={styles.menuOption}>About</span>
        </nav>
        <section>{page}</section>
    </div>
}

export default Box