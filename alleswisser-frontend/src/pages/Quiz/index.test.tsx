import React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import Quiz, { FIRST_QUESTION_ID } from '.'
import { Question } from '../../common/types'
import questions from './TestData'
import userEvent from '@testing-library/user-event';

const [firstQuestion, secondQuestion, thirdQuestion] = questions

describe('Quiz', () => {

  it('gets "yes" or "no" question, depending on pressed button', async () => {
    console.log(firstQuestion)
    console.log(secondQuestion)
    console.log(thirdQuestion)
    const getQuestionMock = jest.fn()
    getQuestionMock.mockResolvedValueOnce(firstQuestion)
    getQuestionMock.mockResolvedValueOnce(secondQuestion)
    getQuestionMock.mockResolvedValue(thirdQuestion)
    await waitFor(() => render(<Quiz getQuestion={getQuestionMock} />))
    const yesButton = screen.getByText('Yes')
    await waitFor(() => yesButton.click())
    const noButton = screen.getByText('No')
    await waitFor(() => noButton.click())
    
    expect(getQuestionMock).toBeCalledWith(FIRST_QUESTION_ID)
    expect(getQuestionMock).toBeCalledWith(firstQuestion.yesId)
    expect(getQuestionMock).toBeCalledWith(secondQuestion.noId)
  })
});
