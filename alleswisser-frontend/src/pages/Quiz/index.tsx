import React, { useEffect, useState } from 'react'
import { Question, GetQuestionFn } from '../../common/types'
import Quiz, { AnsweredQuestion, OnAnswerFn } from './Quiz'

export const FIRST_QUESTION_ID = 0

export type QuizPageProps = {
    getQuestion: GetQuestionFn
}

export default function QuizFeature({getQuestion}: QuizPageProps): JSX.Element {
    let [answeredQuestions, setAnsweredQuestions] = useState<AnsweredQuestion[]>([])
    let [currentQuestion, setCurrentQuestion] = useState<Question|null>()

    useEffect(() => {
        console.log("Evo koristim efekat")
        getQuestion(FIRST_QUESTION_ID).then((v: Question|null) => {
            console.log("evo nasao je pitanje, ali kakvo: " + v)
            setCurrentQuestion(v)
        }, 
        (reason) => {
            console.error("Could not get the first question: " + JSON.stringify(reason))
        })
    }, [getQuestion])

    const onAnswer: OnAnswerFn = (isYes: boolean) => {
        if (currentQuestion == null){
            console.warn("For some reason there is no current question")
            return
        }
        const nextId = isYes ? currentQuestion.yesId : currentQuestion.noId
        getQuestion(nextId).then((v: Question|null) => {
            if (currentQuestion == null) {
                console.warn("Quite impossible situation, there is no current question")
                return
            }
            const answeredQuestion: AnsweredQuestion = {
                question: currentQuestion.question,
                isYes: nextId === currentQuestion.yesId
            }
            setAnsweredQuestions([...answeredQuestions, answeredQuestion])
            setCurrentQuestion(v)
        },
        (reason: any) => {
            console.error("Failed getQuestion id=" + nextId)
        })
    }
    
    return <Quiz answeredQuestions={answeredQuestions} 
                 currentQuestion={currentQuestion?.question} 
                 onAnswer={onAnswer}/>
}