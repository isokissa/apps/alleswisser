import React, { Children } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { expect } from '@storybook/jest';
import { userEvent, waitFor, within } from '@storybook/testing-library';
import Quiz from './Quiz'

export default {
  title: 'Pages/Quiz',
  component: Quiz,
  decorators: [
    (Story) => (
      <div style={{ maxWidth: '600px'}}>
        <Story />
      </div>
    )
  ]
} as ComponentMeta<typeof Quiz>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof Quiz> = (args) => (
  <Quiz {...args} />)

export const Long = Template.bind({})
Long.args = {
  currentQuestion: "Is it a cat",
  answeredQuestions: [
    {
      question: "Is it a living thing",
      isYes: true
    },
    {
      question: `Now comes a bit longer question, just to illustrate what happens when the 
                 text of the question is really long. So, here comes the quesiton: 
                 Does it live in water`,
      isYes: false
    }
  ]
}
