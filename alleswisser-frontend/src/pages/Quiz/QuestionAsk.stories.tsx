import React, { Children } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import QuestionAsk from './QuestionAsk'

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Pages/Quiz/QuestionAsk',
  component: QuestionAsk 
} as ComponentMeta<typeof QuestionAsk>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof QuestionAsk> = (args) => (
<QuestionAsk {...args}>
  {args.children}
</QuestionAsk>)

export const Short = Template.bind({})
Short.args = {
  children: "Is it rectangular?"
}

export const Long = Template.bind({})
Long.args = {
  children: `This is going to be a very long question. First I am going to prepare you for a long 
             question by stating it several times, and only then, when you least expect, I am going 
             to actually ask the question. So, are you ready for a great long question? You have to 
             be fully ready before answering, because if you are not ready, it can happen that you 
             answer wrongly. Assuming that you are ready, I will ask you: is it rectangular?`              
}

