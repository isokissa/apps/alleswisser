import React from 'react'
import { render, screen } from '@testing-library/react'
import Quiz, { AnsweredQuestion } from './Quiz'

describe('Quiz', () => {

  const testQuestions: AnsweredQuestion[] = [
    {
      question: "Is it a living thing",
      isYes: true
    },
    {
      question: "Is it a cat",
      isYes: true
    },
    {
      question: "Is it a tiger",
      isYes: true
    }
  ]

  it('calls back the onAnswer when pressed button', () => {
    const onAnswerMock = jest.fn()
    render(<Quiz onAnswer={onAnswerMock}
      answeredQuestions={testQuestions}
      currentQuestion='Is it a paper tiger' />)
    const yesButton = screen.getByText("Yes", {selector: 'button'})
    const noButton = screen.getByText("No", {selector: 'button'})

    yesButton.click()

    expect(onAnswerMock).toBeCalledWith(true)

    noButton.click()

    expect(onAnswerMock).toBeCalledWith(false)
  })
});
