import { ComponentStory, ComponentMeta } from '@storybook/react'
import testData from './TestData'
import { Question, GetQuestionFn } from '../../common/types'
import Quiz from '.' // NOTE: this is going to get the component from index.tsx

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Pages/Quiz',
  component: Quiz,
  decorators: [
    (Story) => (
      <div style={{ maxWidth: '600px'}}>
        <Story />
      </div>
    )
  ] 
} as ComponentMeta<typeof Quiz>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof Quiz> = (args) => (<Quiz {...args} />)

const getQuestion: GetQuestionFn = (id: number) => {
  return Promise.resolve(testData[id])
} 

export const Playground = Template.bind({})
Playground.args = {
  getQuestion: getQuestion
}

