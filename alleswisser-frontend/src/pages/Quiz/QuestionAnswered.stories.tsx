import React, { Children } from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import QuestionAnswered from './QuestionAnswered'

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/react/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Pages/Quiz/QuestionAnswered',
  component: QuestionAnswered
} as ComponentMeta<typeof QuestionAnswered>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof QuestionAnswered> = (args) => (
<QuestionAnswered isYes={args.isYes}>
  {args.children}
</QuestionAnswered>)

export const Short = Template.bind({})
Short.args = {
  children: "Is it rectangular?",
  isYes: true
}

export const Long = Template.bind({})
Long.args = {
  children: `This is going to be a very long question. First I am going to prepare you for a long 
             question by stating it several times, and only then, when you least expect, I am going 
             to actually ask the question. So, are you ready for a great long question? You have to 
             be fully ready before answering, because if you are not ready, it can happen that you 
             answer wrongly. Assuming that you are ready, I will ask you: is it rectangular?`,
  isYes: false            
}

