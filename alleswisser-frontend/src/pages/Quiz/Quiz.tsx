
import React from 'react'
import QuestionAsk from './QuestionAsk'
import QuestionAnswered from './QuestionAnswered'

export type AnsweredQuestion = {
    question: string,
    isYes: boolean
}

export type OnAnswerFn = (isYes: boolean) => void

type QuizProps = {
    answeredQuestions: AnsweredQuestion[],
    currentQuestion?: string,
    onAnswer: OnAnswerFn
}

export default function Quiz({answeredQuestions, currentQuestion, onAnswer}: QuizProps): JSX.Element {

    const answeredQuestionsList = answeredQuestions.map((a: AnsweredQuestion, i) => (
        <QuestionAnswered key={i} isYes={a.isYes}>{a.question + '?'}</QuestionAnswered>
    ))

    return (
        <React.Fragment>
            {answeredQuestionsList}
            <QuestionAsk onAnswer={onAnswer}>{currentQuestion + '?'}</QuestionAsk>
        </React.Fragment>
    )
}

