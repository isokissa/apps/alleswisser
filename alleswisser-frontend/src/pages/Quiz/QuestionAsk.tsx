import React from 'react'
import Box from '../../components/Box'
import styles from './Question.module.css'

type QuestionAskProps = {
    children: string,
    onAnswer: (isYes: boolean) => void
}

export default function QuestionAsk({ children, onAnswer }: QuestionAskProps): JSX.Element {
    return (
        <Box>
            <div>{children}</div>
            <div className={styles.buttons}>
                <button onClick={() => onAnswer(true)}>Yes</button>
                <button onClick={() => onAnswer(false)}>No</button>
            </div>
        </Box>
    )
}

