import React from 'react'
import Box from '../../components/Box'
import '../../App.css'
import styles from './Question.module.css'

type QuestionAnsweredProps = {
    children: string,
    isYes: boolean,
}

export default function QuesitonAnswered({ children, isYes }: QuestionAnsweredProps): JSX.Element {
    let answer = isYes? "Yes" : "No"
    let classNames = styles.answer + ' ' + styles['answer-' + answer.toLowerCase()]
    return (
        <Box>
            <div className='question'>{children}</div>
            <div className={classNames}>{answer}</div>
        </Box>
    )
}
 