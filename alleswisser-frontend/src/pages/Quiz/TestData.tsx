import Quiz, { FIRST_QUESTION_ID } from '.'
import { Question } from '../../common/types'


const questions: Question[] = [
    {
        id: FIRST_QUESTION_ID,
        parentId: FIRST_QUESTION_ID,
        question: 'Is it a living thing',
        yesId: 1,
        noId: 2
    },
    {
        id: 1,
        parentId: FIRST_QUESTION_ID,
        question: 'Is it a human',
        yesId: 2,
        noId: 3
    },
    {
        id: 2,
        parentId: 1,
        question: 'Is it Felix Nadar',
        yesId: 4,
        noId: 5
    }
]

export default questions
