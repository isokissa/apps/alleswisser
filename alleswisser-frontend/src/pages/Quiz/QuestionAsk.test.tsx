import React from 'react'
import { render, screen } from '@testing-library/react'
import QuestionAsk from './QuestionAsk'


describe('QuestionAsk', () => {



  it('calls callback when yes is pressed', () => {
    const onAnswerMock = jest.fn()
    render(<QuestionAsk onAnswer={onAnswerMock}>Is it the real life</QuestionAsk>)
    const yesButton = screen.getByText('Yes')
    yesButton.click()
  
    expect(onAnswerMock).toBeCalledWith(true)
  })

})
