import { ComponentStory, ComponentMeta } from '@storybook/react'
import { expect } from '@storybook/jest';
import { userEvent, waitFor, within } from '@storybook/testing-library';
import App from './App'

export default {
  title: 'App',
  component: App,
} as ComponentMeta<typeof App>

//👇 We create a “template” of how args map to rendering
const Template: ComponentStory<typeof App> = (args) => (
  <App />)

export const TheWholeApplication = Template.bind({})
