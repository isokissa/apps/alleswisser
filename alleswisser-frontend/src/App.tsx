import React from 'react';
import Layout from './components/Layout'
import Quiz from './pages/Quiz'
import { getQuestion } from './client/questions'
import './App.css';

function App() {
  const quizPage = <Quiz getQuestion={getQuestion} />
  return (
    <Layout page={quizPage} />
  )
}

export default App;
