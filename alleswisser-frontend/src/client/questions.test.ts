import { waitFor } from '@testing-library/react'
import axios, { AxiosResponse } from 'axios'
import { Question } from '../common/types'
import { getQuestion } from './questions'

const id = 1
const dummyQuestion: Question = {
  id: id,
  parentId: 0,
  yesId: id + 1,
  noId: id + 2,
  question: 'test question'  
}

jest.mock('axios')

const axiosMock = jest.mocked(axios, true)

describe('Questions client', () => {
  beforeEach(() => {
    axiosMock.get.mockClear()
  })

  it('returns correct question', async () => {
    const resultExpected: AxiosResponse<Question> = {
      data: dummyQuestion,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {}
    }
    axiosMock.get.mockResolvedValueOnce(resultExpected)

    const result = await waitFor(() => getQuestion(id))
    expect(result).toEqual(dummyQuestion)    
  })

  it('returns null if not found',async () => {
    const resultExpected: AxiosResponse<{}> = {
      data: {},
      status: 404,
      statusText: 'Not found question id = ' + id,
      headers: {},
      config: {}
    }
    axiosMock.get.mockResolvedValueOnce(resultExpected)

    const result = await waitFor(() => getQuestion(id))
    expect(result).toBeNull()
  })

  xit('throws an exception if any other problem with the request than 200 or 404',async () => {
    const resultExpected: AxiosResponse<null> = {
      data: null,
      status: 500,
      statusText: 'Strange error',
      headers: {},
      config: {}
    }
    axiosMock.get.mockRejectedValueOnce(resultExpected)

    const result = await waitFor(() => getQuestion(id))
    expect(result).toBeNull()
  })  
});
