import axios, {AxiosResponse} from 'axios'
import { Question, GetQuestionFn } from '../common/types'

const API_URL = process.env.REACT_APP_ALLESWISSER_API_URL


export const getQuestion: GetQuestionFn = async (id: number) => {
    const url = API_URL + 'questions/get?id=' + id

    try {
        const response: AxiosResponse = await axios.get<Question>(url)
        if (response.status == 200) return response.data
        else return null    
    } catch (error: any) {
        throw new Error(error)
    }
}